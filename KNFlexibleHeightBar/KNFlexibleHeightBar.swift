//
//  KNFlexibleHeightBar.swift
//  BLKFlexibleHeightBar Demo
//
//  Created by Eugene Gubin on 7/20/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

import Foundation
import UIKit

@objc class KNFlexibleHeightBar: UIView {
    
    typealias Bound = (progress: CGFloat, attributes: KNFlexibleHeightBarSubviewLayoutAttributes)
    
    var progress: CGFloat = 0 {
        didSet {
            progress = min(progress, 1)
            if behaviorDefiner == nil || !behaviorDefiner!.elasticMaximumHeightAtTop {
                progress = max(progress, 0)
            }
        }
    }
    
    var maximumBarHeight: CGFloat = 44 {
        didSet {
            maximumBarHeight = max(maximumBarHeight, 0.0)
        }
    }
    
    var minimumBarHeight: CGFloat = 20 {
        didSet {
            minimumBarHeight = max(minimumBarHeight, 0.0)
        }
    }
    
    var behaviorDefiner: KNFlexibleHeightBarBehaviorDefiner? {
        didSet {
            if let bd = behaviorDefiner {
                bd.flexibleHeightBar = self
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        maximumBarHeight = frame.maxY
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        maximumBarHeight = frame.maxY
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var barFrame = frame
        let height = interpolateFromValue(maximumBarHeight, toValue: minimumBarHeight, withProgress: progress)
        frame = CGRect(x: barFrame.origin.x, y: barFrame.origin.y, width: barFrame.width, height: height)
        
        if let bd = behaviorDefiner where bd.elasticMaximumHeightAtTop {
            progress = max(progress, 0)
        }
        
        applyLayoutAttributes()
    }
    
    func applyLayoutAttributes()  {
        
        for subview in subviews as! [UIView] {
            
            for i in 0 ..< subview.numberOfLayoutAttributes {
                
                if let floor = getFloorAtIndex(i, subview: subview),
                   let ceiling = getCeilingAtIndex(i, subview: subview) {
                    
                    applyFloor(floor, ceiling: ceiling, toSubview: subview)
                }
            }
        }
    }
    
    func getFloorAtIndex(index: UInt, subview: UIView) -> Bound? {
        let progress = subview.progressAtIndex(index)
        
        if (self.progress >= progress) {
            let attrs = subview.layoutAttributesAtIndex(index)
            return (progress: progress, attributes: attrs)
        }
        
        return nil
    }
    
    func getCeilingAtIndex(index: UInt, subview: UIView) -> Bound? {
        let attrCount = subview.numberOfLayoutAttributes
        let ceilingIndex = index + 1
        let hasExplicitCeiling = ceilingIndex < attrCount
        
        if hasExplicitCeiling {
            return getExplicitCeilingAtIndex(ceilingIndex, subview: subview)
        } else {
            return getImplicitCeilingAtIndex(index, subview: subview)
        }
    }
    
    func getExplicitCeilingAtIndex(index: UInt, subview: UIView) -> Bound? {
        let progress = subview.progressAtIndex(index)
        
        if (self.progress < progress) {
            
            let attrs = subview.layoutAttributesAtIndex(index)
            return (progress: progress, attributes: attrs)
        }
        
        return nil
    }
    
    func getImplicitCeilingAtIndex(index: UInt, subview: UIView) -> Bound {
        let attrs = subview.layoutAttributesAtIndex(index)
        return (progress: 1.0, attributes: attrs)
    }
    
    func applyFloor(floor: Bound, ceiling: Bound, toSubview subview: UIView) {
        let (floorProgress, floorLayoutAttributes) = floor
        let (ceilingProgress, ceilingLayoutAttributes) = ceiling
        
        let numerator = progress - floorProgress
        let denominator = ceilingProgress == floorProgress ? ceilingProgress : ceilingProgress - floorProgress
        let relativeProgress = numerator / denominator
        
        // Interpolate CA3DTransform
        var transform3D: CATransform3D = CATransform3D()
        transform3D.m11 = interpolateFromValue(floorLayoutAttributes.transform3D.m11, toValue:ceilingLayoutAttributes.transform3D.m11, withProgress:relativeProgress)
        transform3D.m12 = interpolateFromValue(floorLayoutAttributes.transform3D.m12, toValue:ceilingLayoutAttributes.transform3D.m12, withProgress:relativeProgress)
        transform3D.m13 = interpolateFromValue(floorLayoutAttributes.transform3D.m13, toValue:ceilingLayoutAttributes.transform3D.m13, withProgress:relativeProgress)
        transform3D.m14 = interpolateFromValue(floorLayoutAttributes.transform3D.m14, toValue:ceilingLayoutAttributes.transform3D.m14, withProgress:relativeProgress)
        transform3D.m21 = interpolateFromValue(floorLayoutAttributes.transform3D.m21, toValue:ceilingLayoutAttributes.transform3D.m21, withProgress:relativeProgress)
        transform3D.m22 = interpolateFromValue(floorLayoutAttributes.transform3D.m22, toValue:ceilingLayoutAttributes.transform3D.m22, withProgress:relativeProgress)
        transform3D.m23 = interpolateFromValue(floorLayoutAttributes.transform3D.m23, toValue:ceilingLayoutAttributes.transform3D.m23, withProgress:relativeProgress)
        transform3D.m24 = interpolateFromValue(floorLayoutAttributes.transform3D.m24, toValue:ceilingLayoutAttributes.transform3D.m24, withProgress:relativeProgress)
        transform3D.m31 = interpolateFromValue(floorLayoutAttributes.transform3D.m31, toValue:ceilingLayoutAttributes.transform3D.m31, withProgress:relativeProgress)
        transform3D.m32 = interpolateFromValue(floorLayoutAttributes.transform3D.m32, toValue:ceilingLayoutAttributes.transform3D.m32, withProgress:relativeProgress)
        transform3D.m33 = interpolateFromValue(floorLayoutAttributes.transform3D.m33, toValue:ceilingLayoutAttributes.transform3D.m33, withProgress:relativeProgress)
        transform3D.m34 = interpolateFromValue(floorLayoutAttributes.transform3D.m34, toValue:ceilingLayoutAttributes.transform3D.m34, withProgress:relativeProgress)
        transform3D.m41 = interpolateFromValue(floorLayoutAttributes.transform3D.m41, toValue:ceilingLayoutAttributes.transform3D.m41, withProgress:relativeProgress)
        transform3D.m42 = interpolateFromValue(floorLayoutAttributes.transform3D.m42, toValue:ceilingLayoutAttributes.transform3D.m42, withProgress:relativeProgress)
        transform3D.m43 = interpolateFromValue(floorLayoutAttributes.transform3D.m43, toValue:ceilingLayoutAttributes.transform3D.m43, withProgress:relativeProgress)
        transform3D.m44 = interpolateFromValue(floorLayoutAttributes.transform3D.m44, toValue:ceilingLayoutAttributes.transform3D.m44, withProgress:relativeProgress)
        
        // Interpolate frame
        let frame: CGRect
        if !floorLayoutAttributes.frame.isNull && ceilingLayoutAttributes.frame.isNull {
            frame = floorLayoutAttributes.frame
        } else if floorLayoutAttributes.frame.isNull && ceilingLayoutAttributes.frame.isNull {
            frame = subview.frame
        } else {
            let x = interpolateFromValue(floorLayoutAttributes.frame.minX, toValue: ceilingLayoutAttributes.frame.minX, withProgress: relativeProgress)
            let y = interpolateFromValue(floorLayoutAttributes.frame.minY, toValue: ceilingLayoutAttributes.frame.minY, withProgress: relativeProgress)
            let width = interpolateFromValue(floorLayoutAttributes.frame.width, toValue: ceilingLayoutAttributes.frame.width, withProgress: relativeProgress)
            let height = interpolateFromValue(floorLayoutAttributes.frame.height, toValue: ceilingLayoutAttributes.frame.height, withProgress: relativeProgress)
            frame = CGRect(x: x, y: y, width: width, height: height)
        }
        
        // Interpolate center
        let x = interpolateFromValue(floorLayoutAttributes.center.x, toValue: ceilingLayoutAttributes.center.x, withProgress: relativeProgress)
        let y = interpolateFromValue(floorLayoutAttributes.center.y, toValue: ceilingLayoutAttributes.center.y, withProgress: relativeProgress)
        let center = CGPoint(x: x, y: y)
        
        // Interpolate bounds
        let bx = interpolateFromValue(floorLayoutAttributes.bounds.minX, toValue: ceilingLayoutAttributes.bounds.minX, withProgress: relativeProgress)
        let by = interpolateFromValue(floorLayoutAttributes.bounds.minY, toValue: ceilingLayoutAttributes.bounds.minY, withProgress: relativeProgress)
        let bwidth = interpolateFromValue(floorLayoutAttributes.bounds.width, toValue: ceilingLayoutAttributes.bounds.width, withProgress: relativeProgress)
        let bheight = interpolateFromValue(floorLayoutAttributes.bounds.height, toValue: ceilingLayoutAttributes.bounds.height, withProgress: relativeProgress)
        let bounds = CGRect(x: bx, y: by, width: bwidth, height: bheight)
        
        // Interpolate alpha
        let alpha = interpolateFromValue(floorLayoutAttributes.alpha, toValue: ceilingLayoutAttributes.alpha, withProgress: relativeProgress)
        
        // Apply updated attributes
        subview.layer.transform = transform3D
        if CATransform3DIsIdentity(transform3D) {
            subview.frame = frame
        }
        
        subview.center = center;
        subview.bounds = bounds;
        subview.alpha = alpha;
        subview.layer.zPosition = CGFloat(floorLayoutAttributes.zIndex);
        subview.hidden = floorLayoutAttributes.hidden;
    }
    
    func interpolateFromValue(fromValue: CGFloat, toValue: CGFloat, withProgress lprogress: CGFloat) -> CGFloat {
        return fromValue - (fromValue - toValue) * lprogress
    }
}