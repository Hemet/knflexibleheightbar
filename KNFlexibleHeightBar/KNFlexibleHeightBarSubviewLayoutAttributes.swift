//
//  KNFlexibleHeightBarSubviewLayoutAttributes.swift
//  BLKFlexibleHeightBar-Demo
//
//  Created by Eugene Gubin on 7/21/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

import Foundation

/**
The `KNFlexibleHeightBarSubviewLayoutAttributes` class is used to define layout attributes (i.e frame, transform, alpha) for subviews of a `BLKFlexibleHeightBar`.
Note: This class is heavily influenced by UICollectionViewLayoutAttributes.
*/

struct KNFlexibleHeightBarSubviewLayoutAttributes {
    /**
    The frame rectangle of the item.
    The frame rectangle is measured in points and specified in the coordinate system of the collection view. Setting the value of this property also sets the values of the center and size properties.
    */
    private var _frame: CGRect = CGRect.zeroRect
    private var _bounds: CGRect = CGRect.zeroRect
    private var _center: CGPoint = CGPoint.zeroPoint
    private var _size: CGSize = CGSize.zeroSize
    private var _transform3D: CATransform3D = CATransform3DIdentity
    private var _transform: CGAffineTransform = CGAffineTransformIdentity
    
    var frame: CGRect{
        get {
            return _frame
        }
        set {
            if noTransformApplied {
                _frame = newValue;
            }
            
            _center = CGPointMake(CGRectGetMidX(newValue), CGRectGetMidY(newValue))
            _size = CGSizeMake(CGRectGetWidth(newValue), CGRectGetHeight(newValue))
            _bounds = CGRectMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds), size.width, size.height)
        }
    }
    
    var noTransformApplied: Bool {
        return CGAffineTransformIsIdentity(transform) && CATransform3DIsIdentity(transform3D)
    }
    
    /**
    The bounds of the item.
    When setting the bounds, the origin of the bounds rectangle must always be at (0, 0). Changing the bounds rectangle also changes the value in the size property to match the new bounds size.
    */
    var bounds: CGRect {
        get {
            return _bounds
        }
        set {
            assert(newValue.origin.x == 0.0 && newValue.origin.y == 0.0, "Bounds must be set with a (0,0) origin")
            _bounds = newValue
            _size = CGSizeMake(CGRectGetWidth(newValue), CGRectGetHeight(newValue))
        }
    }
    
    /**
    The center point of the item.
    The center point is specified in the coordinate system of the collection view. Setting the value of this property also updates the origin of the rectangle in the frame property.
    */
    var center: CGPoint {
        get {
            return _center
        }
        set {
            _center = newValue
            
            if noTransformApplied {
                _frame = CGRectMake(newValue.x - CGRectGetMidX(bounds), newValue.y - CGRectGetMidY(bounds), CGRectGetWidth(frame), CGRectGetHeight(frame))
            }
        }
    }
    
    /**
    The size of the item.
    Setting the value of this property also changes the size of the rectangle returned by the frame and bounds properties.
    */
    var size: CGSize {
        get {
            return _size
        }
        set {
            _size = newValue
            
            if noTransformApplied {
                _frame = CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), newValue.width, newValue.height)
            }
            
            _bounds = CGRectMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds), newValue.width, newValue.height)
        }
    }
    
    /**
    The 3D transform of the item.
    Assigning a transform other than the identity transform to this property causes the frame property to be set to CGRectNull. Assigning a value also replaces the value in the transform property with an affine version of the 3D transform you specify.
    */
    var transform3D: CATransform3D {
        get {
            return _transform3D
        }
        set {
            _transform3D = newValue
            
            _transform = CGAffineTransformMake(newValue.m11, newValue.m12, newValue.m21, newValue.m22, newValue.m41, newValue.m42);
            
            if !CATransform3DIsIdentity(newValue) {
                _frame = CGRectNull
            }
        }
    }
    
    /**
    The affine transform of the item.
    Assigning a transform other than the identity transform to this property causes the frame property to be set to CGRectNull. Assigning a value also replaces the value in the transform3D property with a 3D version of the affine transform you specify.
    */
    var transform: CGAffineTransform {
        get {
            return _transform
        }
        set {
            _transform3D = CATransform3DMakeAffineTransform(newValue);
            
            if !CGAffineTransformIsIdentity(newValue) {
                _frame = CGRectNull
            }
        }
    }
    
    /**
    The transparency of the item.
    Possible values are between 0.0 (transparent) and 1.0 (opaque). The default is 1.0.
    */
    var alpha: CGFloat = 1
    
    /**
    Specifies the item’s position on the z axis.
    This property is used to determine the front-to-back ordering of items during layout. Items with higher index values appear on top of items with lower values. Items with the same value have an undetermined order.
    
    The default value of this property is 0.
    */
    var zIndex: Int = 0
    
    /**
    Determines whether the item is currently displayed.
    The default value of this property is NO. As an optimization, the collection view might not create the corresponding view if this property is set to YES.
    */
    var hidden: Bool = false
}