//
//  UIView+KNFlexibleHeightBarSubview.swift
//  BLKFlexibleHeightBar-Demo
//
//  Created by Eugene Gubin on 7/21/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

import Foundation
import UIKit

/**
The `KNFlexibleHeightBarSubview` UIView category allows UIViews to define their own layout attributes depending on the progress (height) of their containing `KNFlexibleHeightBar` view. Internally, the UIView instance is mapping progress values to layout attributes instances.

When the KNFlexibleHeightBar lays out its subviews, it will fetch each subview's desired layout attributes depending on the value of the bar's `progress` property. Only layout attributes for key frames need to be specified. The `BLKFlexibleHeightBar` will smoothly and seamlessly transition between layout attributes.
*/
extension UIView {
    private struct AssociatedKeys {
        static let kExtensionContext = malloc(4)
    }
    
    private class ExtenstionContext {
        var progressValues: [CGFloat] = []
        var layoutAttributesForProgressValues: [KNFlexibleHeightBarSubviewLayoutAttributes] = []
    }
    
    /**
    Add layout attributes that correspond to a progress value.
    @param The layout attributes that the receiver wants to be applied to itself.
    @param The progress value (between 0.0 and 1.0 inclusive) that the receiver's containing `BLKFlexibleHeightBar` instance will use to decide which layout attributes to apply.
    */
    func addLayoutAttributes(layoutAttributes: KNFlexibleHeightBarSubviewLayoutAttributes, forProgress progress: CGFloat) {
        // Make sure the progressPosition is between 0 and 1
        let cappedProgress = max(min(progress, 1), 0)
        
        // If layout attributes for this progressPosition already exists, just replace it
        if let progressIndex = find(this.progressValues, cappedProgress) {
            
            this.layoutAttributesForProgressValues[progressIndex] = layoutAttributes
        } else {
            
            var insertionIndex = lastIndexWhere(this.progressValues) { cappedProgress > $0 }
            
            this.progressValues.insert(cappedProgress, atIndex: insertionIndex)
            this.layoutAttributesForProgressValues.insert(layoutAttributes, atIndex: insertionIndex)
        }
    }
    
    func lastIndexWhere<T>(array: [T], predicate: T -> Bool) -> Int {
        var index = 0
        for item in array {
            if predicate(item) {
                index++
            } else {
                break
            }
        }
        return index
    }
    
    /**
    Remove the layout attributes instance that corresponds to then specified progress value.
    @param The progress value corresponding to the layout attributes that are to be removed.
    */
    func removeLayoutAttributesForProgress(progress: CGFloat) {
        if let indexToRemove = find(this.progressValues, progress) {
            this.progressValues.removeAtIndex(indexToRemove)
            this.layoutAttributesForProgressValues.removeAtIndex(indexToRemove)
        }
    }
    
    /**
    Returns the number of layout attributes currently associated with the receiver.
    @return The number of layout attributes currently associated with the receiver.
    */
    var numberOfLayoutAttributes: UInt {
        return UInt(this.layoutAttributesForProgressValues.count)
    }
    
    private var this: ExtenstionContext {
        if let existingContext = objc_getAssociatedObject(self, AssociatedKeys.kExtensionContext) as? ExtenstionContext {
            return existingContext
        }
        
        let newContext = ExtenstionContext()
        objc_setAssociatedObject(self, AssociatedKeys.kExtensionContext, newContext, UInt(OBJC_ASSOCIATION_RETAIN_NONATOMIC))
        return newContext
    }
    
    /**
    Returns the progress value corresponding to the specified index. This is a helper method for `BLKFlexibleHeightBar.`
    @param The index of the desired progress value.
    @return The progress value corresponding to the specified index.
    */
    func progressAtIndex(index: UInt) -> CGFloat {
        return this.progressValues[Int(index)]
    }
    
    /**
    Returns the layout attributes corresponding to the specified index. This is a helper method for `BLKFlexibleHeightBar.`
    @param The index of the desired layout attributes.
    @return The layout attributes corresponding to the specified index.
    */
    func layoutAttributesAtIndex(index: UInt) -> KNFlexibleHeightBarSubviewLayoutAttributes {
        return this.layoutAttributesForProgressValues[Int(index)]
    }
}