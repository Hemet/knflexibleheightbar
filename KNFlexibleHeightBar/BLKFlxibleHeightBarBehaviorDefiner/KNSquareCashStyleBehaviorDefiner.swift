//
//  KNSquareCashStyleBehaviorDefiner.swift
//  BLKFlexibleHeightBar Demo
//
//  Created by Eugene Gubin on 7/20/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

import Foundation
import UIKit

class KNSquareCashStyleBehaviorDefiner: KNFlexibleHeightBarBehaviorDefiner {
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
        
        if !currentlySnapping {
            let progress = (scrollView.contentOffset.y + scrollView.contentInset.top) / (flexibleHeightBar!.maximumBarHeight - self.flexibleHeightBar!.minimumBarHeight)
            flexibleHeightBar!.progress = progress
            flexibleHeightBar!.setNeedsLayout()
        }
    }
}