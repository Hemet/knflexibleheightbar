//
//  KNFlexibleHeightBarBehaviourDefiner.swift
//  BLKFlexibleHeightBar Demo
//
//  Created by Eugene Gubin on 7/20/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

import Foundation
import UIKit

@objc class KNFlexibleHeightBarBehaviorDefiner: NSObject, UIScrollViewDelegate {
    weak var flexibleHeightBar: KNFlexibleHeightBar?
    var snappingEnabled: Bool = true
    var currentlySnapping: Bool = false
    var elasticMaximumHeightAtTop: Bool = false
    
    private var snappingPositionsForProgressRanges: [NSRange:CGFloat] = [:]
    
    func addSnappingPositionProgress(progress: CGFloat, forProgressRangeStart start: CGFloat, end:CGFloat) {
        let progressPercentRange = createRangeForStart(start, end: end)
        
        for (existingRange, _) in snappingPositionsForProgressRanges {
            
            let noRangeConflict = NSIntersectionRange(progressPercentRange, existingRange).length == 0
            assert(noRangeConflict, "progressPercentRange sent to -addSnappingProgressPosition:forProgressPercentRange: intersects a progressPercentRange for an existing progressPosition.")
        }
        
        snappingPositionsForProgressRanges[progressPercentRange] = progress
    }
    
    func removeSnappingPositionProgressForProgressRangeStart(start: CGFloat, end: CGFloat) {
        let progressPercentRange = createRangeForStart(start, end: end)
        
        snappingPositionsForProgressRanges[progressPercentRange] = nil
    }
    
    func snapToProgress(progress: CGFloat, scrollView:UIScrollView) {
        let deltaProgress = progress - flexibleHeightBar!.progress
        let deltaYOffset = (flexibleHeightBar!.maximumBarHeight - flexibleHeightBar!.minimumBarHeight) * deltaProgress
        scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y + deltaYOffset)
        
        
        flexibleHeightBar!.progress = progress
        flexibleHeightBar!.setNeedsLayout()
        flexibleHeightBar!.layoutIfNeeded()
        
        currentlySnapping = false
    }
    
    func snapWithScrollView(scrollView: UIScrollView) {
        if (!currentlySnapping && snappingEnabled && flexibleHeightBar!.progress >= 0) {
            currentlySnapping = true
            
            var snapPosition: CGFloat?
            for (existingRange, position) in snappingPositionsForProgressRanges {
                let progressPercent = Int(flexibleHeightBar!.progress * 100)
                if (progressPercent >= existingRange.location) && (progressPercent <= existingRange.location + existingRange.length) {
                    snapPosition = position
                }
            }
            
            if let snapPosition = snapPosition {
                UIView.animateWithDuration(0.15, animations: { [unowned self] in
                    
                        self.snapToProgress(snapPosition, scrollView: scrollView)
                    
                    }, completion: { [unowned self] finished in
                    
                        self.currentlySnapping = false
                        
                    })
            } else {
                currentlySnapping = false
            }
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        snapWithScrollView(scrollView)
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            snapWithScrollView(scrollView)
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let originIndicatorInsets = scrollView.scrollIndicatorInsets
        scrollView.scrollIndicatorInsets = UIEdgeInsets(top: flexibleHeightBar!.bounds.height,
            left: originIndicatorInsets.left, bottom: originIndicatorInsets.bottom, right: originIndicatorInsets.right)
    }
    
    func createRangeForStart(start: CGFloat, end: CGFloat) -> NSRange {
        // Make sure start and end are between 0 and 1
        let start = capPercentageValue(start)
        let end = capPercentageValue(end)
        return NSMakeRange(Int(start), Int(end - start))
    }
    
    func capPercentageValue(value: CGFloat) -> CGFloat {
        return max(min(value, 1), 0) * 100
    }
    
    func BLK_Private_setFlexibleHeightBar(heightBar: KNFlexibleHeightBar) {
        if flexibleHeightBar != heightBar {
            flexibleHeightBar = heightBar
        }
    }
}

extension NSRange: Hashable {
    public var hashValue: Int {
        // TODO: xor
        return "\(location).\(length)".hashValue
    }
}

public func ==(lhs: NSRange, rhs: NSRange) -> Bool {
    return lhs.hashValue == rhs.hashValue
}