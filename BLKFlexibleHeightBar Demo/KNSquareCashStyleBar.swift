//
//  KNSquareCashStyleViewController.swift
//  BLKFlexibleHeightBar-Demo
//
//  Created by Eugene Gubin on 7/20/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

import Foundation
import UIKit

class KNSquareCashStyleBar: KNFlexibleHeightBar {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        // Configure bar appearence
        maximumBarHeight = 200.0
        minimumBarHeight = 65.0
        backgroundColor = UIColor(red:0.17, green:0.63, blue:0.11, alpha:1)
        
        // Add and configure name label
        let nameLabel = UILabel()
        nameLabel.font = UIFont.systemFontOfSize(22.0)
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.text = "Bryan Keller"
        
        var initialNameLabelLayoutAttributes = KNFlexibleHeightBarSubviewLayoutAttributes()
        initialNameLabelLayoutAttributes.size = nameLabel.sizeThatFits(CGSizeZero)
        initialNameLabelLayoutAttributes.center = CGPoint(x: frame.size.width * 0.5, y: maximumBarHeight - 50.0)
        nameLabel.addLayoutAttributes(initialNameLabelLayoutAttributes, forProgress:0.0)
        
        var midwayNameLabelLayoutAttributes = initialNameLabelLayoutAttributes
            midwayNameLabelLayoutAttributes.center = CGPoint(x: frame.size.width * 0.5, y: (maximumBarHeight - minimumBarHeight) * 0.4 + minimumBarHeight - 50.0)
        nameLabel.addLayoutAttributes(midwayNameLabelLayoutAttributes, forProgress:0.6)
        
        var finalNameLabelLayoutAttributes = midwayNameLabelLayoutAttributes
        finalNameLabelLayoutAttributes.center = CGPoint(x: frame.size.width * 0.5, y: minimumBarHeight - 25.0)
        nameLabel.addLayoutAttributes(finalNameLabelLayoutAttributes, forProgress:1.0)
        
        addSubview(nameLabel)
        
        
        // Add and configure profile image
        let profileImageView = UIImageView(image: UIImage(named: "ProfilePicture.png"))
        profileImageView.contentMode = .ScaleAspectFill
        profileImageView.clipsToBounds = true;
        profileImageView.layer.cornerRadius = 35.0;
        profileImageView.layer.borderWidth = 2.0;
        profileImageView.layer.borderColor = UIColor.whiteColor().CGColor
        
        var initialProfileImageViewLayoutAttributes = KNFlexibleHeightBarSubviewLayoutAttributes()
        initialProfileImageViewLayoutAttributes.size = CGSize(width: 70.0, height: 70.0);
        initialProfileImageViewLayoutAttributes.center = CGPoint(x: frame.size.width * 0.5, y: maximumBarHeight - 110.0)
        profileImageView.addLayoutAttributes(initialProfileImageViewLayoutAttributes, forProgress:0.0)
        
        var midwayProfileImageViewLayoutAttributes = initialProfileImageViewLayoutAttributes
        midwayProfileImageViewLayoutAttributes.center = CGPoint(x: frame.size.width * 0.5, y: (maximumBarHeight - minimumBarHeight) * 0.8 + minimumBarHeight - 110.0)
        profileImageView.addLayoutAttributes(midwayProfileImageViewLayoutAttributes, forProgress:0.2)
        
        var finalProfileImageViewLayoutAttributes = midwayProfileImageViewLayoutAttributes
        finalProfileImageViewLayoutAttributes.center = CGPoint(x: frame.size.width * 0.5, y: (maximumBarHeight - minimumBarHeight) * 0.64 + minimumBarHeight - 110.0);
        finalProfileImageViewLayoutAttributes.transform = CGAffineTransformMakeScale(0.5, 0.5);
        finalProfileImageViewLayoutAttributes.alpha = 0.0;
        profileImageView.addLayoutAttributes(finalProfileImageViewLayoutAttributes, forProgress:0.5)
        
        addSubview(profileImageView)
    }
}